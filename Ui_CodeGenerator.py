#! /usr/bin/python

#################################################################
# Project: PyQt Code Generator
# Filename: Ui_CodeGenerator.py
# Description: An useful tool for PyQt source code generating
# Date: 2009.05.25
# Programmer: Lin Xin Yu
# E-mail: xinyu0123@gmail.com
# Website:http://nxforce.blogspot.com
#################################################################

#================= import modules ===============================

from PyQt4 import QtCore, QtGui

#================= Ui_CodeGeneratorClass Start ==================

class Ui_CodeGeneratorClass(object):
	def setupUi(self, CodeGeneratorClass):
		CodeGeneratorClass.setObjectName("CodeGeneratorClass")
		CodeGeneratorClass.resize(600, 600)
		
		# Project information groupbox
		self.groupBox_proj = QtGui.QGroupBox(CodeGeneratorClass)
		self.groupBox_proj.setGeometry(QtCore.QRect(10, 10, 580, 220))
		self.groupBox_proj.setObjectName("groupBox_proj")
		
		# Project Name label
		self.label_project_main = QtGui.QLabel(self.groupBox_proj)
		self.label_project_main.setGeometry(QtCore.QRect(10, 30, 60, 16))
		self.label_project_main.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
		self.label_project_main.setObjectName("label_project_main")
		
		# Project name lineEdit
		self.lineEdit_project_main = QtGui.QLineEdit(self.groupBox_proj)
		self.lineEdit_project_main.setGeometry(QtCore.QRect(80, 30, 490, 25))
		self.lineEdit_project_main.setObjectName("lineEdit_project_main")
		
		# UI type label
		self.label_uiType = QtGui.QLabel(self.groupBox_proj)
		self.label_uiType.setGeometry(QtCore.QRect(10, 65, 60, 15))
		self.label_uiType.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
		self.label_uiType.setObjectName("label_uiType")
		
		# export DirDialog
		self.fileDialog_loadUI = QtGui.QFileDialog(CodeGeneratorClass)
		
		# UI type combobox
		self.comboBox_uiType = QtGui.QComboBox(self.groupBox_proj)
		self.comboBox_uiType.setGeometry(QtCore.QRect(80, 65, 490, 25))
		self.comboBox_uiType.setEditable(False)
		self.comboBox_uiType.setObjectName("comboBox_uiType")
		self.comboBox_uiType.insertItem(0, 'QWidget')
		self.comboBox_uiType.insertItem(1, 'OMainWindow')
		self.comboBox_uiType.insertItem(2, 'ODialog')
		self.comboBox_uiType.insertItem(3, '<Load .ui file>')
		
		# modules label
		self.label_module = QtGui.QLabel(self.groupBox_proj)
		self.label_module.setGeometry(QtCore.QRect(10, 100, 60, 15))
		self.label_module.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
		self.label_module.setObjectName("label_module")
		
		# modules gridlayout
		self.gridLayoutWidget = QtGui.QWidget(self.groupBox_proj)
		self.gridLayoutWidget.setGeometry(QtCore.QRect(70, 90, 510, 140))
		self.gridLayoutWidget.setObjectName("gridLayoutWidget")
		self.gridLayout = QtGui.QGridLayout(self.gridLayoutWidget)
		self.gridLayout.setObjectName("gridLayout")
		
		# QtGui
		self.checkBox_gui = QtGui.QCheckBox(self.gridLayoutWidget)
		self.checkBox_gui.setObjectName("checkBox_gui")
		self.checkBox_gui.setEnabled(False)
		self.checkBox_gui.setChecked(True)
		self.gridLayout.addWidget(self.checkBox_gui, 1, 0, 1, 1)
		
		# QtOpenGL
		self.checkBox_opengl = QtGui.QCheckBox(self.gridLayoutWidget)
		self.checkBox_opengl.setObjectName("checkBox_opengl")
		self.gridLayout.addWidget(self.checkBox_opengl, 1, 1, 1, 1)
		
		# QtNetwork
		self.checkBox_net = QtGui.QCheckBox(self.gridLayoutWidget)
		self.checkBox_net.setObjectName("checkBox_net")
		self.gridLayout.addWidget(self.checkBox_net, 0, 1, 1, 1)
		
		# QtCore
		self.checkBox_core = QtGui.QCheckBox(self.gridLayoutWidget)
		self.checkBox_core.setObjectName("checkBox_core")
		self.checkBox_core.setEnabled(False)
		self.checkBox_core.setChecked(True)
		self.gridLayout.addWidget(self.checkBox_core, 0, 0, 1, 1)
		
		# QtScript
		self.checkBox_script = QtGui.QCheckBox(self.gridLayoutWidget)
		self.checkBox_script.setObjectName("checkBox_script")
		self.gridLayout.addWidget(self.checkBox_script, 1, 2, 1, 1)
		
		# QtXml
		self.checkBox_xml = QtGui.QCheckBox(self.gridLayoutWidget)
		self.checkBox_xml.setObjectName("checkBox_xml")
		self.gridLayout.addWidget(self.checkBox_xml, 0, 2, 1, 1)
		
		# QtXmlPatterns
		self.checkBox_xmlPatterns = QtGui.QCheckBox(self.gridLayoutWidget)
		self.checkBox_xmlPatterns.setObjectName("checkBox_xmlPatterns")
		self.gridLayout.addWidget(self.checkBox_xmlPatterns, 0, 3, 1, 1)
		
		# QtWebKit
		self.checkBox_web = QtGui.QCheckBox(self.gridLayoutWidget)
		self.checkBox_web.setObjectName("checkBox_web")
		self.gridLayout.addWidget(self.checkBox_web, 2, 1, 1, 1)
		
		# QtSvg
		self.checkBox_svg = QtGui.QCheckBox(self.gridLayoutWidget)
		self.checkBox_svg.setObjectName("checkBox_svg")
		self.gridLayout.addWidget(self.checkBox_svg, 2, 2, 1, 1)
		
		# QtScriptTools
		self.checkBox_scriptTools = QtGui.QCheckBox(self.gridLayoutWidget)
		self.checkBox_scriptTools.setObjectName("checkBox_scriptTools")
		self.gridLayout.addWidget(self.checkBox_scriptTools, 1, 3, 1, 1)
		
		# Phonon
		self.checkBox_phonon = QtGui.QCheckBox(self.gridLayoutWidget)
		self.checkBox_phonon.setObjectName("checkBox_phonon")
		self.gridLayout.addWidget(self.checkBox_phonon, 2, 3, 1, 1)
		
		# QtSql
		self.checkBox_sql = QtGui.QCheckBox(self.gridLayoutWidget)
		self.checkBox_sql.setObjectName("checkBox_sql")
		self.gridLayout.addWidget(self.checkBox_sql, 2, 0, 1, 1)

		# QtMultimedia
		self.checkBox_multimedia = QtGui.QCheckBox(self.gridLayoutWidget)
		self.checkBox_multimedia.setObjectName("checkBox_multimedia")
		self.gridLayout.addWidget(self.checkBox_multimedia, 3, 0, 1, 1)

		# QtOpenVG
		self.checkBox_openvg = QtGui.QCheckBox(self.gridLayoutWidget)
		self.checkBox_openvg.setObjectName("checkBox_openvg")
		self.checkBox_openvg.setEnabled(False)
		self.gridLayout.addWidget(self.checkBox_openvg, 4, 1, 1, 1)
		
		# Qt3Support
		self.checkBox_qt3support = QtGui.QCheckBox(self.gridLayoutWidget)
		self.checkBox_qt3support.setObjectName("checkBox_qt3support")
		self.checkBox_qt3support.setEnabled(False)
		self.gridLayout.addWidget(self.checkBox_qt3support, 4, 2, 1, 1)

		# QtDesigner
		self.checkBox_designer = QtGui.QCheckBox(self.gridLayoutWidget)
		self.checkBox_designer.setObjectName("checkBox_qtdesigner")
		self.gridLayout.addWidget(self.checkBox_designer, 3, 3, 1, 1)

		# QtUiTools
		self.checkBox_uitools = QtGui.QCheckBox(self.gridLayoutWidget)
		self.checkBox_uitools.setObjectName("checkBox_qtuitools")
		self.checkBox_uitools.setEnabled(False)
		self.gridLayout.addWidget(self.checkBox_uitools, 4, 0, 1, 1)

		# QtHelp
		self.checkBox_help= QtGui.QCheckBox(self.gridLayoutWidget)
		self.checkBox_help.setObjectName("checkBox_qthelp")
		self.gridLayout.addWidget(self.checkBox_help, 3, 1, 1, 1)

		# QtTest
		self.checkBox_test = QtGui.QCheckBox(self.gridLayoutWidget)
		self.checkBox_test.setObjectName("checkBox_qttest")
		self.gridLayout.addWidget(self.checkBox_test, 3, 2, 1, 1)

		# QAxContainer
		self.checkBox_qaxcontainer = QtGui.QCheckBox(self.gridLayoutWidget)
		self.checkBox_qaxcontainer.setObjectName("checkBox_qaxcontainer")
		self.checkBox_qaxcontainer.setEnabled(False)
		self.gridLayout.addWidget(self.checkBox_qaxcontainer, 4, 3, 1, 1)

		# QAxServer
		self.checkBox_qaxserver = QtGui.QCheckBox(self.gridLayoutWidget)
		self.checkBox_qaxserver.setObjectName("checkBox_qaxserver")
		self.checkBox_qaxserver.setEnabled(False)
		self.gridLayout.addWidget(self.checkBox_qaxserver, 5, 0, 1, 1)

		# QtDBus
		self.checkBox_dbus = QtGui.QCheckBox(self.gridLayoutWidget)
		self.checkBox_dbus.setObjectName("checkBox_dbus")
		self.checkBox_dbus.setEnabled(False)
		self.gridLayout.addWidget(self.checkBox_dbus, 5, 1, 1, 1)
		
		# comment groupbox
		self.groupBox_comt = QtGui.QGroupBox(CodeGeneratorClass)
		self.groupBox_comt.setGeometry(QtCore.QRect(10, 240, 580, 320))
		self.groupBox_comt.setCheckable(True)
		self.groupBox_comt.setChecked(False)
		self.groupBox_comt.setObjectName("groupBox_comt")
		
		# project label
		self.label_project = QtGui.QLabel(self.groupBox_comt)
		self.label_project.setGeometry(QtCore.QRect(-10, 30, 90, 16))
		self.label_project.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
		self.label_project.setObjectName("label_project")
		
		# project lineEdit		
		self.lineEdit_projName = QtGui.QLineEdit(self.groupBox_comt)
		self.lineEdit_projName.setGeometry(QtCore.QRect(90, 30, 480, 23))
		self.lineEdit_projName.setObjectName("lineEdit_projName")
		
		# Version label
		self.label_version = QtGui.QLabel(self.groupBox_comt)
		self.label_version.setGeometry(QtCore.QRect(20, 60, 60, 15))
		self.label_version.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
		self.label_version.setObjectName("label_version")
		
		# version lineEdit
		self.lineEdit_version = QtGui.QLineEdit(self.groupBox_comt)
		self.lineEdit_version.setGeometry(QtCore.QRect(90, 60, 480, 23))
		self.lineEdit_version.setObjectName("lineEdit_version")
		
		# Author label
		self.label_author = QtGui.QLabel(self.groupBox_comt)
		self.label_author.setGeometry(QtCore.QRect(30, 90, 50, 20))
		self.label_author.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
		self.label_author.setObjectName("label_author")
		
		# Author lineEdit		
		self.lineEdit_author = QtGui.QLineEdit(self.groupBox_comt)
		self.lineEdit_author.setGeometry(QtCore.QRect(90, 90, 480, 23))
		self.lineEdit_author.setObjectName("lineEdit_author")
		
		# E-mail label
		self.label_mail = QtGui.QLabel(self.groupBox_comt)
		self.label_mail.setGeometry(QtCore.QRect(40, 120, 40, 20))
		self.label_mail.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
		self.label_mail.setObjectName("label_mail")
		
		# E-mail lineEdit
		self.lineEdit_mail = QtGui.QLineEdit(self.groupBox_comt)
		self.lineEdit_mail.setGeometry(QtCore.QRect(90, 120, 480, 23))
		self.lineEdit_mail.setObjectName("lineEdit_mail")
		
		# Website label
		self.label_website = QtGui.QLabel(self.groupBox_comt)
		self.label_website.setGeometry(QtCore.QRect(20, 150, 60, 15))
		self.label_website.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
		self.label_website.setObjectName("label_website")
		
		# Website lineEdit
		self.lineEdit_website = QtGui.QLineEdit(self.groupBox_comt)
		self.lineEdit_website.setGeometry(QtCore.QRect(90, 150, 480, 23))
		self.lineEdit_website.setObjectName("lineEdit_website")
		
		# Description Label 
		self.label_description = QtGui.QLabel(self.groupBox_comt)
		self.label_description.setGeometry(QtCore.QRect(0, 180, 80, 16))
		self.label_description.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
		self.label_description.setObjectName("label_description")		
		
		# Description TextEdit
		self.plainTextEdit_description = QtGui.QPlainTextEdit(self.groupBox_comt)
		self.plainTextEdit_description.setGeometry(QtCore.QRect(90, 180, 480, 131))
		self.plainTextEdit_description.setObjectName("plainTextEdit_description")
		
		# Export Icon
		self.folderIcon = QtGui.QIcon("icon/Folder.png")
		
		# export path button
		self.pushButton_path = QtGui.QPushButton(CodeGeneratorClass)
		self.pushButton_path.setGeometry(QtCore.QRect(20, 565, 35, 25))
		self.pushButton_path.setObjectName("pushButton_path")
		self.pushButton_path.setIcon(self.folderIcon)
		
		# export DirDialog
		self.fileDialog_export = QtGui.QFileDialog(CodeGeneratorClass)
		
		# export path lineEdit
		self.lineEdit_path = QtGui.QLineEdit(CodeGeneratorClass)
		self.lineEdit_path.setGeometry(QtCore.QRect(60, 565, 425, 25))
		self.lineEdit_path.setObjectName("lineEdit_path")
		
		# 'Generate' button
		self.pushButton_generate = QtGui.QPushButton(CodeGeneratorClass)
		self.pushButton_generate.setGeometry(QtCore.QRect(490, 565, 90, 25))
		self.pushButton_generate.setObjectName("pushButton_generate")

		self.retranslateUi(CodeGeneratorClass)
		
		# signals and slots
		QtCore.QObject.connect(self.pushButton_generate, QtCore.SIGNAL("clicked()"), CodeGeneratorClass.onGenerateClicked)
		QtCore.QObject.connect(self.pushButton_path, QtCore.SIGNAL("clicked()"), CodeGeneratorClass.onExportClicked)
		QtCore.QObject.connect(self.lineEdit_path, QtCore.SIGNAL("returnPressed()"), CodeGeneratorClass.onExportSetted)
		QtCore.QObject.connect(self.lineEdit_path, QtCore.SIGNAL("textChanged(const QString &)"), CodeGeneratorClass.onExportTextChanged)
		QtCore.QObject.connect(self.lineEdit_project_main, QtCore.SIGNAL("returnPressed()"), CodeGeneratorClass.onProjectSeted)
		QtCore.QObject.connect(self.lineEdit_version, QtCore.SIGNAL("returnPressed()"), CodeGeneratorClass.onVersionSeted)
		QtCore.QObject.connect(self.lineEdit_mail, QtCore.SIGNAL("returnPressed()"), CodeGeneratorClass.onMailSeted)
		QtCore.QObject.connect(self.lineEdit_author, QtCore.SIGNAL("returnPressed()"), CodeGeneratorClass.onAuthorSeted)
		QtCore.QObject.connect(self.lineEdit_website, QtCore.SIGNAL("returnPressed()"), CodeGeneratorClass.onWebsiteSeted)		
		QtCore.QObject.connect(self.plainTextEdit_description, QtCore.SIGNAL("returnPressed()"), CodeGeneratorClass.onDescriptionSeted)		
		QtCore.QObject.connect(self.lineEdit_project_main, QtCore.SIGNAL("textChanged(QString)"), CodeGeneratorClass.onProjectChanged)
		QtCore.QObject.connect(self.comboBox_uiType, QtCore.SIGNAL("currentIndexChanged(int)"), CodeGeneratorClass.onUITypeChanged)
		QtCore.QMetaObject.connectSlotsByName(CodeGeneratorClass)

	def retranslateUi(self, CodeGeneratorClass):
		CodeGeneratorClass.setWindowTitle(QtGui.QApplication.translate("CodeGeneratorClass", "PyQt Code Generator", None, QtGui.QApplication.UnicodeUTF8))
		self.groupBox_proj.setTitle(QtGui.QApplication.translate("CodeGeneratorClass", "Project Infomation", None, QtGui.QApplication.UnicodeUTF8))
		self.label_project_main.setText(QtGui.QApplication.translate("CodeGeneratorClass", "Porject:", None, QtGui.QApplication.UnicodeUTF8))
		self.label_uiType.setText(QtGui.QApplication.translate("CodeGeneratorClass", "UI Type:", None, QtGui.QApplication.UnicodeUTF8))
		self.label_module.setText(QtGui.QApplication.translate("CodeGeneratorClass", "Modules:", None, QtGui.QApplication.UnicodeUTF8))
		self.checkBox_gui.setText(QtGui.QApplication.translate("CodeGeneratorClass", "QtGui", None, QtGui.QApplication.UnicodeUTF8))
		self.checkBox_opengl.setText(QtGui.QApplication.translate("CodeGeneratorClass", "QtOpenGL", None, QtGui.QApplication.UnicodeUTF8))
		self.checkBox_net.setText(QtGui.QApplication.translate("CodeGeneratorClass", "QtNetwork", None, QtGui.QApplication.UnicodeUTF8))
		self.checkBox_core.setText(QtGui.QApplication.translate("CodeGeneratorClass", "QtCore", None, QtGui.QApplication.UnicodeUTF8))
		self.checkBox_script.setText(QtGui.QApplication.translate("CodeGeneratorClass", "QtScript", None, QtGui.QApplication.UnicodeUTF8))
		self.checkBox_xml.setText(QtGui.QApplication.translate("CodeGeneratorClass", "QtXml", None, QtGui.QApplication.UnicodeUTF8))
		self.checkBox_xmlPatterns.setText(QtGui.QApplication.translate("CodeGeneratorClass", "QtXmlPatters", None, QtGui.QApplication.UnicodeUTF8))
		self.checkBox_web.setText(QtGui.QApplication.translate("CodeGeneratorClass", "QtWebKit", None, QtGui.QApplication.UnicodeUTF8))
		self.checkBox_svg.setText(QtGui.QApplication.translate("CodeGeneratorClass", "QtSvg", None, QtGui.QApplication.UnicodeUTF8))
		self.checkBox_scriptTools.setText(QtGui.QApplication.translate("CodeGeneratorClass", "QtScriptTools", None, QtGui.QApplication.UnicodeUTF8))
		self.checkBox_phonon.setText(QtGui.QApplication.translate("CodeGeneratorClass", "Phonon", None, QtGui.QApplication.UnicodeUTF8))
		self.checkBox_sql.setText(QtGui.QApplication.translate("CodeGeneratorClass", "QtSql", None, QtGui.QApplication.UnicodeUTF8))
		self.checkBox_multimedia.setText(QtGui.QApplication.translate("CodeGeneratorClass", "QtMultimedia", None, QtGui.QApplication.UnicodeUTF8))
		self.checkBox_openvg.setText(QtGui.QApplication.translate("CodeGeneratorClass", "QtOpenVG", None, QtGui.QApplication.UnicodeUTF8))
		self.checkBox_qt3support.setText(QtGui.QApplication.translate("CodeGeneratorClass", "Qt3Support", None, QtGui.QApplication.UnicodeUTF8))
		self.checkBox_designer.setText(QtGui.QApplication.translate("CodeGeneratorClass", "QtDesigner", None, QtGui.QApplication.UnicodeUTF8))
		self.checkBox_uitools.setText(QtGui.QApplication.translate("CodeGeneratorClass", "QtUiTools", None, QtGui.QApplication.UnicodeUTF8))
		self.checkBox_help.setText(QtGui.QApplication.translate("CodeGeneratorClass", "QtHelp", None, QtGui.QApplication.UnicodeUTF8))
		self.checkBox_test.setText(QtGui.QApplication.translate("CodeGeneratorClass", "QtTest", None, QtGui.QApplication.UnicodeUTF8))
		self.checkBox_qaxcontainer.setText(QtGui.QApplication.translate("CodeGeneratorClass", "QAxContainer", None, QtGui.QApplication.UnicodeUTF8))
		self.checkBox_qaxserver.setText(QtGui.QApplication.translate("CodeGeneratorClass", "QAxServer", None, QtGui.QApplication.UnicodeUTF8))
		self.checkBox_dbus.setText(QtGui.QApplication.translate("CodeGeneratorClass", "QtDBus", None, QtGui.QApplication.UnicodeUTF8))
		self.groupBox_comt.setTitle(QtGui.QApplication.translate("CodeGeneratorClass", "Comment", None, QtGui.QApplication.UnicodeUTF8))
		self.label_project.setText(QtGui.QApplication.translate("CodeGeneratorClass", "Porject:", None, QtGui.QApplication.UnicodeUTF8))
		self.label_description.setText(QtGui.QApplication.translate("CodeGeneratorClass", "Description:", None, QtGui.QApplication.UnicodeUTF8))
		self.label_version.setText(QtGui.QApplication.translate("CodeGeneratorClass", "Version:", None, QtGui.QApplication.UnicodeUTF8))
		self.label_author.setText(QtGui.QApplication.translate("CodeGeneratorClass", "Author:", None, QtGui.QApplication.UnicodeUTF8))
		self.label_mail.setText(QtGui.QApplication.translate("CodeGeneratorClass", "E-mail:", None, QtGui.QApplication.UnicodeUTF8))
		self.label_website.setText(QtGui.QApplication.translate("CodeGeneratorClass", "Website:", None, QtGui.QApplication.UnicodeUTF8))
		self.pushButton_generate.setText(QtGui.QApplication.translate("CodeGeneratorClass", "Generate", None, QtGui.QApplication.UnicodeUTF8))

#================= Ui_CodeGeneratorClass End ==================
