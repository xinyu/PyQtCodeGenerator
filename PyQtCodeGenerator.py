#! /usr/local/bin/python

#################################################################
# Project: PyQt Code Generator
# Filename: PyQtCodeGenerator.py
# Description: An useful tool for PyQt Code generating
# Date: 2009.05.25
# Programmer: Lin Xin Yu
# E-mail: xinyu0123@gmail.com
# Website:http://code.200u.com
#################################################################

#================= import modules ===============================

import sys
import CodeGeneratorClass
from PyQt4 import QtCore, QtGui

#================= Main Start ===================================

if __name__ == '__main__':
	app = QtGui.QApplication(sys.argv)
	codeGenerator = CodeGeneratorClass.CodeGeneratorClass()
	codeGenerator.show()
	sys.exit(app.exec_())
	
#================= Main End =====================================