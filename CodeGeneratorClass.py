#! /usr/bin/python

#################################################################
# Project: PyQt Code Generator
# Filename: CodeGeneratorClass.py
# Description: An useful tool for PyQt source code generating
# Date: 2009.05.25
# Programmer: Lin Xin Yu
# E-mail: xinyu0123@gmail.com
# Website:http://nxforce.blogspot.com
#################################################################

#================= import modules ===============================

import os
import sys
import datetime
import textwrap
import XmlParseHandler
import Ui_CodeGenerator
from PyQt4 import QtCore, QtGui
from xml.sax import make_parser, SAXException
from xml.sax.handler import ContentHandler

#================= CodeGenerator Class Start ====================

class CodeGeneratorClass(QtGui.QWidget):
	def __init__(self, parent = None):
		QtGui.QWidget.__init__(self, parent)
		self.ui = Ui_CodeGenerator.Ui_CodeGeneratorClass()
		self.ui.setupUi(self)

		self.exportPath = ''

		self.uiPath = ''
		self.uiClassName = ''
		self.uiType = 'QWidget'
		self.uiIndex = 0

		self.comment = ''
		self.modules = ''

		self.project = ''
		self.version = ''
		self.date = ''
		self.email = ''
		self.author = ''
		self.website= ''
		self.description = ''

		# load log
		self.loadLog()

	#=== SLOT for 'Generate' button
	def onGenerateClicked(self):
		if(self.ui.lineEdit_project_main.text()):

			self.ui.lineEdit_project_main.emit(QtCore.SIGNAL("returnPressed()"))
			self.ui.lineEdit_version.emit(QtCore.SIGNAL("returnPressed()"))
			self.ui.lineEdit_author.emit(QtCore.SIGNAL("returnPressed()"))
			self.ui.lineEdit_mail.emit(QtCore.SIGNAL("returnPressed()"))
			self.ui.lineEdit_website.emit(QtCore.SIGNAL("returnPressed()"))
			self.ui.plainTextEdit_description.emit(QtCore.SIGNAL("returnPressed()"))
			self.date = datetime.datetime.now().ctime()

			# check if the comment section is selected
			if(self.ui.groupBox_comt.isChecked()):
				self.generateComment()
				cmtEnabled = True
			else:
				cmtEnabled = False

			# generate modules
			self.generateModules()

			# call the exporting button slot for asking the exporting path
			if(self.exportPath == ''):
				self.onExportClicked()

			# make sure that the exporting path had been configured
			if(self.exportPath):

				# Generate the ui code by pre-defined xml-based ui file, make sure you do have the pyuic4 program in you system
				if(self.uiIndex == 3):
					cmd = 'pyuic4 "' + self.uiPath + '" >> "' + self.exportPath + '/Ui_' + self.uiClassName + '.py"'
					os.system(cmd)

				# generate ui code by this program
				else:
					self.uiClassName = self.project.replace(' ', '')+'Class'
					self.generateUICode(self.exportPath+'/'+'Ui_'+self.project.replace(' ', '')+'Class.py', cmtEnabled)

				# generate project class code
				self.generateProjectCode(self.exportPath+'/'+self.project.replace(' ', '')+'Class.py', cmtEnabled)

				# generate main code
				self.generateMainCode(self.exportPath+'/'+self.project.replace(' ', '')+'.py', cmtEnabled)

				QtGui.QMessageBox.information(self, 'Message', "   Done!   ", QtGui.QMessageBox.Ok)

	#=== SLOT for UIType combobox
	def onUITypeChanged(self, index):
		self.uiIndex = index
		if(index == 0):
			self.uiType = 'QWidget'
			return None

		if(index == 1):
			self.uiType = 'QMainWindow'
			return None

		if(index == 2):
			self.uiType = 'QDialog'
			return None

		if (index == 3):
			filename = self.ui.fileDialog_loadUI.getOpenFileName(self, 'Open file', QtCore.QDir.homePath()+'/Desktop', "UI files(*.ui)")

			if(filename):
				self.ui.comboBox_uiType.setItemText(3, filename)
				self.uiPath = str(filename)

				# call xml parser for ui file
				xmlParseHandler = XmlParseHandler.XmlParseHandler(sys.stdout)
				xmlParser = make_parser()
				xmlParser.setContentHandler(xmlParseHandler)

				file = open(self.uiPath, 'r')

				try:
					xmlParser.parse(file)
				except SAXException:
					self.uiType = xmlParseHandler.uiType
					self.uiClassName = xmlParseHandler.className

				file.close()

			else:
				# if user didn't specify any ui file, switch back to default uiType
				if(str(self.ui.comboBox_uiType.itemText(3)) == '<Load .ui file>'):
					self.ui.comboBox_uiType.setCurrentIndex(0)
					self.uiIndex = 0

	#=== function for generating project class code
	def generateProjectCode(self, filename, cmtEnabled):
		projClass = self.project.replace(' ', '')+'Class'

		file = open(filename, 'w')
		file.write('#! /usr/bin/python\n\n')
		if(cmtEnabled):
			file.write(self.comment)
		file.write('\n')
		file.write('#'*15+' import modules '.ljust(55,'#')+'\n\n')
		file.write('import Ui_'+self.uiClassName+'\n')
		file.write(self.modules+'\n')
		file.write('\n')
		file.write('#'*15+(' '+projClass+' Start'+' ').ljust(55,'#')+'\n\n')
		file.write('class '+projClass+'(QtGui.'+self.uiType+'):\n')
		file.write('\tdef __init__(self, parent = None):\n')
		file.write('\t\tQtGui.'+self.uiType+'.__init__(self, parent)\n')
		file.write('\t\tself.ui = '+'Ui_'+self.uiClassName+'.Ui_'+self.uiClassName+'()'+'\n')
		file.write('\t\tself.ui.setupUi(self)\n\n\n')
		file.write('#'*15+(' '+projClass+' End'+' ').ljust(55,'#')+'\n\n')
		file.close()

	#=== function for generating UI code
	def generateUICode(self, filename, cmtEnabled):

		file = open(filename, 'w')
		file.write('#! /usr/bin/python\n\n')
		if(cmtEnabled):
			file.write(self.comment)
		file.write('\n')
		file.write('#'*15+' import modules '.ljust(55,'#')+'\n\n')
		file.write(self.modules+'\n\n')
		file.write('#'*15+(' Ui_'+self.uiClassName+' start ').ljust(55,'#')+'\n\n')
		file.write('class '+'Ui_'+self.uiClassName+'(object):'+'\n')
		file.write('\tdef setupUi(self, '+self.uiClassName+'):'+'\n')
		file.write('\t\t'+self.uiClassName+'.resize(400,300)'+'\n')
		file.write('\n')

		if(self.uiType == 'QMainWindow'):
			file.write('\t\tself.centralWidget = QtGui.QWidget('+self.uiClassName+')\n')
			file.write('\t\t'+self.uiClassName+'.setCentralWidget(self.centralWidget)\n')
			file.write('\n')
			file.write('\t\tself.menubar = QtGui.QMenuBar('+self.uiClassName+')\n')
			file.write('\t\tself.menubar.setGeometry(QtCore.QRect(0, 0, 400, 20))\n')
			file.write('\t\t'+self.uiClassName+'.setMenuBar(self.menubar)\n')
			file.write('\n')
			file.write('\t\tself.statusbar = QtGui.QStatusBar('+self.uiClassName+')\n')
			file.write('\t\t'+self.uiClassName+'.setStatusBar(self.statusbar)\n')
			file.write('\n')

		file.write('\t\tself.retranslateUi('+self.uiClassName+')\n')
		file.write('\t\tQtCore.QMetaObject.connectSlotsByName('+self.uiClassName+')\n')
		file.write('\n')
		file.write('\tdef retranslateUi(self, '+self.uiClassName+'):\n')
		file.write('\t\t'+self.uiClassName+'.setWindowTitle(QtGui.QApplication.translate(\"'+self.uiClassName+'\", \"'+self.project+'\", None, QtGui.QApplication.UnicodeUTF8))')
		file.write('\n\n')
		file.write('#'*15+(' Ui_'+self.uiClassName+' end ').ljust(55,'#')+'\n')
		file.close()

	#=== function for generating main code
	def generateMainCode(self, filename, cmtEnabled):
		mainProg = self.project.replace(' ', '')

		file = open(filename, 'w')
		file.write('#! /usr/bin/python\n\n')
		if(cmtEnabled):
			file.write(self.comment)
		file.write('\n')
		file.write('#'*15+' import modules '.ljust(55,'#')+'\n\n')
		file.write('import sys\n')
		file.write('import '+mainProg+'Class\n')
		file.write(self.modules+'\n\n')
		file.write('#'*15+(' '+mainProg+' start ').ljust(55,'#')+'\n\n')
		file.write('if __name__ == \'__main__\':\n')
		file.write('\tapp = QtGui.QApplication(sys.argv)\n')
		file.write('\t'+mainProg+'Obj = '+mainProg+'Class.'+mainProg+'Class()\n')
		file.write('\t'+mainProg+'Obj.show()\n')
		file.write('\tsys.exit(app.exec_())\n\n')
		file.write('#'*15+(' '+mainProg+' start ').ljust(55,'#')+'\n')
		file.close()

	#=== function for generating comments
	def generateComment(self):
		self.comment = ''
		self.comment += '#'*70 + '\n'
		self.comment += '#\n'
		self.comment += '#'+'Project: '.rjust(15) + self.project + '\n'
		self.comment += '#'+'Version: '.rjust(15) + self.version + '\n'
		self.comment += '#'+'Date: '.rjust(15) + self.date + '\n'
		self.comment += '#'+'Author: '.rjust(15) + self.author + '\n'
		self.comment += '#'+'E-mail: '.rjust(15) + self.email + '\n'
		self.comment += '#'+'WebSite: '.rjust(15) + self.website + '\n'
		self.comment += '#'+'Description: '.rjust(15)+'\n'

		strList = textwrap.wrap(self.description, 55)

		for line in strList:
			self.comment += '#'+' '.rjust(15) + line + '\n'

		self.comment += '#\n'
		self.comment += '#'*70 + '\n'

	#=== function for generating modules
	def generateModules(self):
		self.modules = 'from PyQt4 import QtCore, QtGui'

		if(self.ui.checkBox_opengl.isChecked()):
			self.modules += ', QtOpenGL'

		if(self.ui.checkBox_net.isChecked()):
			self.modules += ', QtNetwork'

		if(self.ui.checkBox_script.isChecked()):
			self.modules += ', QtScript'

		if(self.ui.checkBox_xml.isChecked()):
			self.modules += ', QtXml'

		if(self.ui.checkBox_xmlPatterns.isChecked()):
			self.modules += ', QtXmlPatterns'

		if(self.ui.checkBox_web.isChecked()):
			self.modules += ', QtWebKit'

		if(self.ui.checkBox_svg.isChecked()):
			self.modules += ', QtSvg'

		if(self.ui.checkBox_scriptTools.isChecked()):
			self.modules += ', QtScriptTools'

		if(self.ui.checkBox_sql.isChecked()):
			self.modules += ', QtSql'

		if(self.ui.checkBox_phonon.isChecked()):
			self.modules += ', Phonon'

		if(self.ui.checkBox_multimedia.isChecked()):
			self.modules += ', QtMultimedia'

		if(self.ui.checkBox_openvg.isChecked()):
			self.modules += ', QtOpenVG'

		if(self.ui.checkBox_qt3support.isChecked()):
			self.modules += ', Qt3Support'

		if(self.ui.checkBox_designer.isChecked()):
			self.modules += ', QtDesigner'

		if(self.ui.checkBox_uitools.isChecked()):
			self.modules += ', QtUiTools'

		if(self.ui.checkBox_help.isChecked()):
			self.modules += ', QtHelp'

		if(self.ui.checkBox_test.isChecked()):
			self.modules += ', QtTest'

		if(self.ui.checkBox_qaxcontainer.isChecked()):
			self.modules += ', QAxContainer'

		if(self.ui.checkBox_qaxserver.isChecked()):
			self.modules += ', QAxServer'

		if(self.ui.checkBox_dbus.isChecked()):
			self.modules += ', QtDBus'

	#=== load log file
	def saveLog(self):
		try:
			file = open('CodeGeneratorLog.log', 'w')

			self.onAuthorSeted()
			self.onMailSeted()
			self.onWebsiteSeted()

			file.write(self.author+'\n')
			file.write(self.email+'\n')
			file.write(self.website+'\n')

			file.close()
		except IOError:
			print ("Fail to open file for writing")

	#=== save log file
	def loadLog(self):
		try:
			file = open('CodeGeneratorLog.log')

			data = file.read().split('\n')

			self.ui.lineEdit_author.setText(data[0])
			self.ui.lineEdit_mail.setText(data[1])
			self.ui.lineEdit_website.setText(data[2])

			file.close()
		except IOError:
			print ("Log file does not exist, exiting gracefully")

	#=== SLOT for export path button
	def onExportClicked(self):
		self.exportPath = self.ui.fileDialog_export.getExistingDirectory(self, 'Open File', QtCore.QDir.homePath()+'/Desktop')
		self.ui.lineEdit_path.setText(self.exportPath)
		self.exportPath = (self.exportPath)

	#=== SLOT for export path lineEdit
	def onExportTextChanged(self, string):
		self.exportPath = (string)

	#=== SLOT for export path lineEdit
	def onExportSetted(self):
		self.exportPath = (self.ui.lineEdit_path.text())

	#=== SLOT for main project name in Project information groupbox
	def onProjectChanged(self):
		self.ui.lineEdit_projName.setText(self.ui.lineEdit_project_main.text())

	#=== SLOT for main project name in comment groupbox
	def onProjectSeted(self):
		self.project = (self.ui.lineEdit_project_main.text())

	#=== SLOT for version
	def onVersionSeted(self):
		self.version = (self.ui.lineEdit_version.text())

	#=== SLOT for mail
	def onMailSeted(self):
		self.email = (self.ui.lineEdit_mail.text())

	#=== SLOT for website
	def onWebsiteSeted(self):
		self.website= (self.ui.lineEdit_website.text())

	#=== SLOT for author
	def onAuthorSeted(self):
		self.author = (self.ui.lineEdit_author.text())

	#=== SLOT for description
	def onDescriptionSeted(self):
		self.description = (self.ui.plainTextEdit_description.toPlainText())

	#=== SLOT for close event
	def closeEvent(self, event):
		self.saveLog()

#================= CodeGenerator Class End ======================
