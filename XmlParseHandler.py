#! /usr/bin/python

#################################################################
# Project: PyQt Code Generator
# Filename: XmlParseHandler.py
# Description: An useful tool for PyQt source code generating
# Date: 2009.05.25
# Programmer: Lin Xin Yu
# E-mail: xinyu0123@gmail.com
# Website:http://nxforce.blogspot.com
#################################################################

#================= import modules ===============================

from xml.sax.handler import ContentHandler
from xml.sax import make_parser, SAXException

#================= XmlParseHandler Start ========================

class XmlParseHandler(ContentHandler):
	def __init__(self, fileHandler):
		self.buffer = ''
		self.isUiType = 0
		
	def startElement(self, name, attrs):
		if name == 'widget':
			#if attrs.get('class') == 'QWidget' or attrs.get('class') == 'QMainWindow':
			self.uiType = attrs.get('class')
			self.className = attrs.get('name')
			self.isUiType = 1
	
	def endElement(self, name):
		if self.isUiType:
			self.isUiType = 0
			raise SAXException('Found Class Name and Widget')
	
	def characters(self, chars):
		if self.isUiType: self.buffer += chars

#================= XmlParseHandler End ==========================
